# Onta API

El objetivo de este proyecto es albergar el código de backend del proyecto
onta, actualmente escrito en django.

Su objetivo es responder a las siguientes preguntas:

* ¿qué rutas existen?
* ¿Qué rutas existen cuyos nombres se parecen a esta cadena?
* ¿Qué paradas existen?
* ¿Qué rutas pasan por esta parada?
* ¿Cuáles son las paradas más cercanas a este punto?
* ¿Qué camión me puede llevar si voy de aquí a allá?

Las preguntas y respuestas deben ser accesibles mediante una API rest.

Adicionalmente se ofrece una interfaz mediante la administración de django para
manejar las rutas y paradas contenidas en la base de datos.

Para la búsqueda de nombres de rutas se va a usar búsqueda full-text de postgres.

## Desarrollo

### Requisitos

el almacenamiento de las rutas es hará usando postgis para explotar sus
características de manejo de objetos espaciales.

* python 3.5+
* postgres
* postgis (se instala postgis en el SO y se activa en la base de datos usando `CREATE EXTENSION postgis`.
* gnu gettext

### Proceso

* instalar dependencias usando `pipenv install --three`
* verificar el paso anterior corriendo `pipenv run python --version`. Tiene que salir 3.5 o mayor
* crear un archivo de configuraciones `settings_local.py` en la raíz, configurar base de datos postgres.
* crear un archivo `.env` donde se especifique la variable de entorno `DJANGO_SETTINGS_MODULE` apuntando al archivo de configuración del paso anterior
* `python manage.py migrate`
* `python manage.py runserver`
* crea un usuario para desarrollo con `python manage.py createsuperuser`
* visita el sitio en `http://localhost:8000` e inicia sesión con el usuario recién creado

A continuación una sugerencia de archivo `settings_local.py`

```python
from onta.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'dbname',
        'USER': 'dbuser',
        'HOST': 'localhost',
        # 'PASSWORD': 'dbpass',
    }
}
```

y una sugerencia de archivo `.env`

```
DJANGO_SETTINGS_MODULE=settings_local
```

### Instalar postgres y postgis con docker (opcional)

En caso de que tengas problemas para instalar estos servicios, como
alternativa puedes usar docker con la imagen
`mdillon/postgis`. Asumiendo que ya tienes instalado `docker`, solo
necesitas dos pasos:

#### Crear un _volumen_ para almacenar los datos de postgres

```
docker volume create onta_dev
```

#### Ejecutar la imagen de postgis

```
docker run -p 5432:5432 -e POSTGRES_USER=nombre_usario -e POSTGRES_DB=onta \
   -e POSTGRES_PASSWORD=12345  mdillon/postgis
```

solo cambia los valores de las variables `POSTGRES_USER`,
`POSTGRES_DB` y `POSTGRES_PASSWORD` con los valores que hayas definido
en `settings_local.py`. Puedes probar la conexión con el cliente de
postgres como lo harías normalmente:

```
psql onta -U nombre_usuario -h 127.0.0.1
```

## Importar datos a la base de datos

### Rutas

puedes importar cualquier cantidad de rutas en formato geojson corriendo este
comando:

```bash
manage.py load_routes ruta1.geojson ruta2.geojson
```

puedes encontrar rutas de ejemplo en https://github.com/xalapacode/xalapa_bus_data
